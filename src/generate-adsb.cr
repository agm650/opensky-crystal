# TODO: Write documentation for `Adsb`

require "json"
require "sqlite3"

# CLI option parsing
require "option_parser"

# config = "config.txt"
json_input : String? = nil # = "test2.json"
db_path : String? = nil    # = "/Volumes/ramdisk/adsb.sqlite"
clean_db = false

max_plan_id : Int64

last_pos_array = Hash(Int64, {Float64, Float64, Int64}).new

start_time : Time = Time.utc
end_time : Time

main_parser = OptionParser.new do |parser|
  parser.banner = "Usage: generate-adsb [arguments]"
  parser.on("-i <path to json>", "--input=<path to json>", "JSON input file to use to populate the database") { |path| json_input = path }
  parser.on("-o <sqlite_db_path>", "--output=<sqlite_db_path>", "Path to the sqlite database.") { |path| db_path = path }
  parser.on("-p", "--purge", "Do we want to purge an existing sqlite DB") { clean_db = true }
  parser.on("-h", "--help", "Show this help") { puts parser; exit 0 }
end

main_parser.parse

if db_path.nil? || json_input.nil?
  # Invalid parameters
  puts main_parser.to_s
  exit 1
end

# Check if database exist.
if !File.exists?(db_path.to_s)
  # if it doesn't exist, create it
  db = DB.open "sqlite3://#{db_path}"

  db.exec "pragma synchronous = OFF"
  db.exec "pragma journal_mode = MEMORY"
  db.exec "pragma temp_store = MEMORY"
  db.exec "pragma locking_mode = EXCLUSIVE"
  db.exec "pragma auto_vacuum = 1"

  db.exec "CREATE TABLE 'flights' (
    'id'  INTEGER NOT NULL DEFAULT 1 PRIMARY KEY AUTOINCREMENT UNIQUE,
    'icao24'  TEXT,
    'callsign'  TEXT NOT NULL,
    'first'  INTEGER NOT NULL,
    'last'  INTEGER NOT NULL
  );"
  db.exec "CREATE TABLE 'positions' (
    'id'  INTEGER NOT NULL DEFAULT 1 PRIMARY KEY AUTOINCREMENT UNIQUE,
    'fid'  INTEGER NOT NULL,
    'latitude'  REAL NOT NULL,
    'longitude'  REAL NOT NULL,
    'baro_altitude'  REAL,
    'on_ground'  INTEGER NOT NULL,
    'velocity'  REAL,
    'true_track'  REAL,
    'vertical_rate'  REAL,
    'geo_altitude'  REAL,
    'squawk'  INTEGER,
    'spi'  INTEGER,
    'timestamp'  INTEGER NOT NULL,
    FOREIGN KEY('fid') REFERENCES flights('id')
  );"
  db.exec "PRAGMA auto_vacuum = 1;"
  db.exec "vacuum;"
else
  db = DB.open "sqlite3://#{db_path}"
end

json_data = File.open(json_input.to_s)

raw_data = JSON.parse(json_data).as_a

if clean_db
  db.exec "pragma synchronous = OFF"
  db.exec "pragma journal_mode = MEMORY"
  db.exec "pragma temp_store = MEMORY"
  db.exec "pragma locking_mode = EXCLUSIVE"
  db.exec "pragma auto_vacuum = 1"

  db.exec "delete from flights"
  db.exec "delete from positions"
end

existing_callsign = Hash(String, Int64).new

cpt = 0
cur_vector = 0
skp_vector = 0
max_items = raw_data.size

STDOUT.printf "Progress:     "

insert_into_flight = db.build_prepared_statement "insert into flights ('id', 'icao24','callsign','first','last') values (?, ?, ?, ?, ?)"
update_last_seen = db.build_prepared_statement "update flights SET last=? WHERE callsign=?"
insert_position = db.build_prepared_statement "insert into positions \
        ('fid', 'latitude','longitude','baro_altitude','on_ground', 'velocity', 'true_track', 'vertical_rate', 'geo_altitude', 'squawk', 'spi', 'timestamp') \
        values \
        (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
count_flight = db.build_prepared_statement "select COUNT(id) from flights"
count_positions = db.build_prepared_statement "select COUNT(id) from positions"
# get_last_id = db.build_prepared_statement "select id from flights WHERE callsign=?"

# Calculate the next max_plan_id
count_fdr : Int64 = count_flight.scalar.as(Int64)
if count_fdr > 0
  max_plan_id = db.scalar("select MAX(id) from flights").as(Int64)
  max_plan_id = max_plan_id + 1
else
  max_plan_id = 1
end

db.exec "BEGIN TRANSACTION"

raw_data.each do |rec|
  cpt += 1

  STDOUT.printf "\e[4D%03d%%", (cpt.to_f / (max_items.to_f / 100.to_f)).to_i
  STDOUT.flush

  rec = rec.as_h
  time_of_rec = rec["time"].as_i
  states = rec["states"].as_a

  states.each do |vector|
    # plan_id : Int64
    data = vector.as_a

    cur_vector += 1

    icao24 = data[0].as_s.strip
    callsign = data[1].as_s.strip
    # origin_country = data[2].as_s.strip
    time_position = data[3].as_i?
    # last_contact = data[4].as_i?
    longitude = data[5].as_f?
    latitude = data[6].as_f?
    baro_altitude = data[7].as_f?
    on_ground = data[8].as_bool?
    velocity = data[9].as_f?
    true_track = data[10].as_f?
    vertical_rate = data[11].as_f?
    # sensors = data[12] # Sensor array
    geo_altitude = data[13].as_f?
    squawk = data[14].as_s?
    spi = data[15].as_bool?
    # position_source = data[16].as_i?

    squawk_i : Int32
    if !squawk.nil?
      begin
        squawk_i = squawk.strip.to_i(8)
      rescue
        squawk_i = 0
      end
    else
      squawk_i = 0
    end

    if callsign.size == 0
      skp_vector += 1
      next
    end

    if !existing_callsign.has_key?(callsign)
      existing_callsign[callsign] = max_plan_id
      fid = max_plan_id
      max_plan_id += 1
      # STDOUT.puts "#{time_of_rec} - #{icao24} - #{callsign} - #{squawk_i} -  #{latitude}/#{longitude}"
      insert_into_flight.exec max_plan_id, icao24, callsign, time_of_rec, time_of_rec
    else
      update_last_seen.exec time_of_rec, callsign
      fid = existing_callsign[callsign]
    end

    # Storing positions reports
    # First retrieve the last position if any
    if last_pos_array.has_key?(fid)
      result = last_pos_array[fid]
    else
      # No previous record
      result = {0.0.to_f64, 0.0.to_f64, 0.to_i64}
    end

    if time_of_rec == result[0] || (longitude == result[2] && latitude == result[1])
      skp_vector += 1
      next
    end

    if fid && latitude && longitude
      insert_position.exec fid, latitude, longitude, baro_altitude, on_ground, velocity, true_track, vertical_rate, geo_altitude, squawk_i, spi, time_position
      last_pos_array[fid] = Tuple.new(latitude, longitude, time_of_rec.to_i64)
    end
  end
end

db.exec "COMMIT TRANSACTION"

stats_f = count_flight.scalar.as(Int64)
stats_p = count_positions.scalar.as(Int64)
STDOUT.puts "Finished!"
STDOUT.puts "#{stats_f} flights in DB with #{stats_p} points"
STDOUT.puts "Disarded #{skp_vector}/#{cur_vector} vectors"
db.close

end_time = Time.utc
STDOUT.puts "Elapsed time: #{end_time.to_unix - start_time.to_unix} seconds\n"
