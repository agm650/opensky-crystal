module TRS::Soprano
  class TRS::Soprano::Config
    getter scenrario_name : String
    getter now_timestamp : Int64
    getter creation_timestamp : Int64
    getter duration : Int64

    def self.instance
      @@instance ||= new
    end

    def initialize(@scenrario_name : String, now : Int64, creation : Int64, @duration : Int64 = 86400000)
      @now_timestamp = now * 1_000
      @creation_timestamp = creation * 1_000
    end

    def generate_xml
      begin
        config_file = File.open("#{@scenrario_name}/ScnConfig.xml", "w")
      rescue except
        STDERR.puts "Exception!\n#{except.message}\n"
        exit -1
      end

      config_file.printf("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n")
      config_file.printf("<ScnConfig>\n")
      config_file.printf("\t<Models>\n")
      config_file.printf("\t\t<RadarModel>PERFECT</RadarModel>\n")
      config_file.printf("\t\t<EarthModel>WGS_84</EarthModel>\n")
      config_file.printf("\t</Models>\n")
      config_file.printf("\t<Time>%d</Time>\n", @creation_timestamp)
      config_file.printf("\t<Comment>Scenario Created for NRG purpose</Comment>\n")
      config_file.printf("\t<CreationDate>%d</CreationDate>\n", @now_timestamp)
      config_file.printf("\t<ModificationDate>%d</ModificationDate>\n", @now_timestamp)
      config_file.printf("\t<Duration>%d</Duration>\n", @duration)
      config_file.printf("\t<SimIndic>NO</SimIndic>\n")
      config_file.printf("\t<GeoPoint>\n")
      config_file.printf("\t\t<Latitude>N460000</Latitude>\n")
      config_file.printf("\t\t<Longitude>E0020000</Longitude>\n")
      config_file.printf("\t</GeoPoint>\n")
      config_file.printf("\t<SopranoName>SESP Scenario</SopranoName>\n")
      config_file.printf("\t<SopranoOwner>Dandoy Luc</SopranoOwner>\n")
      config_file.printf("\t<SopranoVersion>1.0</SopranoVersion>\n")
      config_file.printf("</ScnConfig>\n\r")

      config_file.close
    end
  end
end
