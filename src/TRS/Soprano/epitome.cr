module TRS::Soprano
  class TRS::Soprano::Epitome
    getter epitome_file : File

    def self.instance
      @@instance ||= new
    end

    def initialize(@scenrario_name : String)
      @epitome_file = File.open("#{scenrario_name}/epitome.txt", "w")

      @epitome_file.printf("Air trajectories\n")
    end

    def add_plane_to_epitome(callsign : String)
      @epitome_file.printf("  %s; Air\n    Command\n", callsign)
    end

    def add_takeoff_to_epitome(now : Time, hour : Int, min : Int, sec : Int, bearing : Int, adep : String = "AFIL")
      @epitome_file.printf(
        "    Take off (Base); %02d/%02d/%04d-%02d:%02d:%02d; %d°; %s\n",
        now.day,
        now.month,
        now.year,
        hour,
        min,
        sec,
        bearing,
        adep)
    end

    def add_point_to_epitome(speed, level, latitude, longitude)
      @epitome_file.printf(
        "    Point; *; %04d.00 KTS; %03d.00 FL; %s    %s\n",
        speed,
        level,
        latitude,
        longitude)
    end

    def add_land_base_to_epitome(ades)
      @epitome_file.printf("    Landing (Base); #{ades}\n")
    end

    def close_epitome
      @epitome_file.printf("\n")
      @epitome_file.close
    end
  end
end
