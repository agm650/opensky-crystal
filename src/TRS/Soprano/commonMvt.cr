module TRS::Soprano
  struct TRS::Soprano::AirMvt
    getter pos_x
    getter pos_y
    getter bearing
    getter level_type
    getter level
    getter level_unit
    getter speed_type
    getter speed
    getter speed_unit
    getter iff3

    def initialize(@pos_x : String, # Soprano Type position
                   @pos_y : String, # Soprano Type position
                   @bearing : Float64,
                   @level_type : String,
                   @level : Int64?,
                   @level_unit : String,
                   @speed_type : String,
                   @speed : Int64?,
                   @speed_unit : String,
                   @iff3 : Int64)
    end
  end

  class CommonMvt
    property commonmvt_xml_file : File
    property commonmvt_xml_file_name = File

    def self.instance
      @@instance ||= new
    end

    def initialize(scenario_name : String)
      @commonmvt_xml_file = File.open("#{scenario_name}/ScnCommonMvmtInfo.xml", "w")
      @commonmvt_xml_file.printf("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n<ScnCommonMvmtList>\n")
    end

    def add_air_ptf_mvt(callsign : String)
      @commonmvt_xml_file.printf("\t<ScnCommonMvmtInfo>\n")
      @commonmvt_xml_file.printf("\t\t<Name>air@#{callsign}</Name>\n")
      @commonmvt_xml_file.printf("\t\t<ScnProfile>\n")
      @commonmvt_xml_file.printf("\t\t\t<CommandsList>\n")
    end

    def end_air_ptf_mvt
      @commonmvt_xml_file.printf("\t\t\t</CommandsList>\n")
      @commonmvt_xml_file.printf("\t\t</ScnProfile>\n")
      @commonmvt_xml_file.printf("\t</ScnCommonMvmtInfo>\n")
    end

    def add_air_ptf_init_point(timestamp : Int, point : AirMvt)
      init_tstamp = timestamp * 1_000
      @commonmvt_xml_file.printf("\t\t\t\t<AirPlatformActionInit>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t<SimActionBase>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t<ActionExecutionMode>StartedTimeExec</ActionExecutionMode>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t<StartedTime>#{init_tstamp}</StartedTime>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t<OnBeginEventsList>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t</OnBeginEventsList>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t<OnEndEventsList>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t</OnEndEventsList>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t</SimActionBase>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t<GeoPoint>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t<Latitude>#{point.pos_x}</Latitude>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t<Longitude>#{point.pos_y}</Longitude>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t</GeoPoint>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t<Heading>#{point.bearing}</Heading>\n")
      if !point.level.nil?
        @commonmvt_xml_file.printf("\t\t\t\t\t<SimLevelParam>\n")
        @commonmvt_xml_file.printf("\t\t\t\t\t\t<LevelType>#{point.level_type}</LevelType>\n")
        @commonmvt_xml_file.printf("\t\t\t\t\t\t<LevelValue>#{point.level}</LevelValue>\n")
        @commonmvt_xml_file.printf("\t\t\t\t\t\t<LevelUnit>#{point.level_unit}</LevelUnit>\n")
        @commonmvt_xml_file.printf("\t\t\t\t\t</SimLevelParam>\n")
      end
      if !point.speed.nil?
        @commonmvt_xml_file.printf("\t\t\t\t\t<SimSpeedParam>\n")
        @commonmvt_xml_file.printf("\t\t\t\t\t\t<SpeedType>#{point.speed_type}</SpeedType>\n")
        @commonmvt_xml_file.printf("\t\t\t\t\t\t<SpeedValue>#{point.speed}</SpeedValue>\n")
        @commonmvt_xml_file.printf("\t\t\t\t\t\t<SpeedUnit>#{point.speed_unit}</SpeedUnit>\n")
        @commonmvt_xml_file.printf("\t\t\t\t\t</SimSpeedParam>\n")
      end
      @commonmvt_xml_file.printf("\t\t\t\t</AirPlatformActionInit>\n")
    end

    def add_air_ptf_take_off(timestamp : Int, adep : String, bearing : Float)
      init_tstamp = timestamp * 1_000
      @commonmvt_xml_file.printf("\t\t\t\t<AirPlatformActionTakeOff>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t<SimActionBase>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t<ActionExecutionMode>StartedTimeExec</ActionExecutionMode>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t<StartedTime>%d</StartedTime>\n", init_tstamp)
      @commonmvt_xml_file.printf("\t\t\t\t\t\t<OnBeginEventsList>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t</OnBeginEventsList>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t<OnEndEventsList>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t</OnEndEventsList>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t</SimActionBase>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t<PointType>AIRPORT</PointType>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t<Airport>%s</Airport>\n", adep)
      @commonmvt_xml_file.printf("\t\t\t\t\t<Heading>%f</Heading>\n", bearing)
      @commonmvt_xml_file.printf("\t\t\t\t</AirPlatformActionTakeOff>\n")
    end

    def add_air_ptf_land_base(ades : String, bearing : Float)
      @commonmvt_xml_file.printf("\t\t\t\t<AirPlatformActionLand>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t<SimActionBase>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t<ActionExecutionMode>SequentialExec</ActionExecutionMode>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t<OnBeginEventsList>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t</OnBeginEventsList>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t<OnEndEventsList>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t</OnEndEventsList>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t</SimActionBase>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t<PointType>AIRPORT</PointType>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t<Airport>%s</Airport>\n", ades)
      @commonmvt_xml_file.printf("\t\t\t\t\t<Heading>%f</Heading>\n", bearing)
      @commonmvt_xml_file.printf("\t\t\t\t</AirPlatformActionLand>\n")
    end

    def add_air_ptf_waypoint(point : AirMvt)
      @commonmvt_xml_file.printf("\t\t\t\t<AirPlatformActionWayPoint>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t<SimActionBase>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t<ActionExecutionMode>SequentialExec</ActionExecutionMode>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t<OnBeginEventsList>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t</OnBeginEventsList>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t<OnEndEventsList>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t</OnEndEventsList>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t</SimActionBase>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t<GeoPoint>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t<Latitude>#{point.pos_x}</Latitude>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t<Longitude>#{point.pos_y}</Longitude>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t</GeoPoint>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t<PointType>POINT</PointType>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t<CrossConstraint>NO</CrossConstraint>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t<CrossDate></CrossDate>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t<LevelConstraint>YES</LevelConstraint>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t<SimLevelParameters>\n")
      if !point.level.nil?
        @commonmvt_xml_file.printf("\t\t\t\t\t\t<SimLevelParam>\n")
        @commonmvt_xml_file.printf("\t\t\t\t\t\t\t<LevelType>%s</LevelType>\n", point.level_type)
        @commonmvt_xml_file.printf("\t\t\t\t\t\t\t<LevelValue>%d</LevelValue>\n", point.level)
        @commonmvt_xml_file.printf("\t\t\t\t\t\t\t<LevelUnit>%s</LevelUnit>\n", point.level_unit)
        @commonmvt_xml_file.printf("\t\t\t\t\t\t</SimLevelParam>\n")
      end
      @commonmvt_xml_file.printf("\t\t\t\t\t\t<SimRocdParam>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t\t<RocdType></RocdType>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t\t<RocdValue></RocdValue>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t</SimRocdParam>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t<SimVertAccelParam>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t\t<VertAccelType></VertAccelType>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t\t<VertAccelValue></VertAccelValue>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t</SimVertAccelParam>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t</SimLevelParameters>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t<SpeedConstraint>YES</SpeedConstraint>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t<SimSpeedParameters>\n")
      if !point.speed.nil?
        @commonmvt_xml_file.printf("\t\t\t\t\t\t<SimSpeedParam>\n")
        @commonmvt_xml_file.printf("\t\t\t\t\t\t\t<SpeedType>#{point.speed_type}</SpeedType>\n")
        @commonmvt_xml_file.printf("\t\t\t\t\t\t\t<SpeedValue>#{point.speed}</SpeedValue>\n")
        @commonmvt_xml_file.printf("\t\t\t\t\t\t\t<SpeedUnit>#{point.speed_unit}</SpeedUnit>\n")
        @commonmvt_xml_file.printf("\t\t\t\t\t\t</SimSpeedParam>\n")
      end
      @commonmvt_xml_file.printf("\t\t\t\t\t\t<SimTgtAccelParam>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t\t<TgtAccelType></TgtAccelType>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t\t<TgtAccelValue></TgtAccelValue>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t\t<TgtAccelUnit></TgtAccelUnit>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t</SimTgtAccelParam>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t</SimSpeedParameters>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t<TurnRateConstraint>NO</TurnRateConstraint>\n")
      @commonmvt_xml_file.printf("\t\t\t\t</AirPlatformActionWayPoint>\n")
    end

    def add_air_ptf_end_wpt(point : AirMvt)
      @commonmvt_xml_file.printf("\t\t\t\t<AirPlatformActionWayPoint>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t<SimActionBase>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t<ActionExecutionMode>SequentialExec</ActionExecutionMode>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t<OnBeginEventsList>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t</OnBeginEventsList>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t<OnEndEventsList>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t</OnEndEventsList>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t</SimActionBase>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t<GeoPoint>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t<Latitude>%s</Latitude>\n", point.pos_x)
      @commonmvt_xml_file.printf("\t\t\t\t\t\t<Longitude>%s</Longitude>\n", point.pos_y)
      @commonmvt_xml_file.printf("\t\t\t\t\t</GeoPoint>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t<PointType>POINT</PointType>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t<CrossConstraint>NO</CrossConstraint>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t<CrossDate></CrossDate>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t<LevelConstraint>NO</LevelConstraint>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t<SpeedConstraint>NO</SpeedConstraint>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t<TurnRateConstraint>NO</TurnRateConstraint>\n")
      @commonmvt_xml_file.printf("\t\t\t\t</AirPlatformActionWayPoint>\n")
    end

    def add_air_ptf_orbit(orb_name : String, nb_lap : Int, direction : String = "LEFT")
      @commonmvt_xml_file.printf("\t\t\t\t<AirPlatformActionOrbit>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t<SimActionBase>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t<ActionExecutionMode>SequentialExec</ActionExecutionMode>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t<OnBeginEventsList>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t</OnBeginEventsList>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t<OnEndEventsList>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t</OnEndEventsList>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t</SimActionBase>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t<Orbit>%s</Orbit>\n", orb_name)
      @commonmvt_xml_file.printf("\t\t\t\t\t<Direction>%s</Direction>\n", direction)
      @commonmvt_xml_file.printf("\t\t\t\t\t<Nb_Laps>%d</Nb_Laps>\n", nb_lap)
      @commonmvt_xml_file.printf("\t\t\t\t\t<CrossConstraint>NO</CrossConstraint>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t<ApproachMode>MANUAL</ApproachMode>\n")
      @commonmvt_xml_file.printf("\t\t\t\t</AirPlatformActionOrbit>\n")
    end

    def add_air_ptf_hold(radius : Float, nb_lap : Int, direction : String = "RIGHT")
      @commonmvt_xml_file.printf("\t\t\t\t<AirPlatformActionHold>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t<SimActionBase>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t<ActionExecutionMode>SequentialExec</ActionExecutionMode>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t<OnBeginEventsList>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t</OnBeginEventsList>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t<OnEndEventsList>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t\t</OnEndEventsList>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t</SimActionBase>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t<Radius>%f</Radius>\n", radius)
      @commonmvt_xml_file.printf("\t\t\t\t\t<RadiusType>RADIUS</RadiusType>\n")
      @commonmvt_xml_file.printf("\t\t\t\t\t<Direction>%s</Direction>\n", direction)
      @commonmvt_xml_file.printf("\t\t\t\t\t<Nb_Laps>%d</Nb_Laps>\n", nb_lap)
      @commonmvt_xml_file.printf("\t\t\t\t</AirPlatformActionHold>\n")
    end

    def close_file
      @commonmvt_xml_file.printf("</ScnCommonMvmtList>\n\r")
      @commonmvt_xml_file.close
    end
  end
end
