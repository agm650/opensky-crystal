module TRS::Soprano
  struct SopFPL
    def initialize(@callsign : String,
                   @actype : String,
                   @iff3 : Int64,
                   @originator : String,
                   @flrules : String,
                   @fltype : String,
                   @acnum : Int64,
                   @turbulence : String,
                   @equip : String,
                   @survequip : String,
                   @date : Int64,
                   @eet : Int64,
                   @adep : String,
                   @ades : String,
                   @alt1 : String,
                   @alt2 : String,
                   @rfl_type : String,
                   @rfl_val : Int64,
                   @rfl_unit : String,
                   @speed_type : String,
                   @speed_val : Int64,
                   @speed_unit : String)
    end
  end

  class TRS::Soprano::FlightPlan
    property flp_data = {} of String => NamedTuple(plan: SopFPL, position: Array(String))
    getter scenrario_name : String

    def self.instance
      @@instance ||= new
    end

    def initialize
      @scenrario_name = ""
    end

    def get_nb_fpl
    end

    def add_fpl(fpl : SopFPL)
      flp_data[fpl.callsign][:plan] = fpl
    end

    def add_point(callsign : String, position : String)
      flp_data[callsign][:position] = position
    end

    def generate_xml(scn_name : String)
      begin
        fpl_file = File.open("#{scn_name}/ScnFlightPlan.xml", "w")
      rescue except
        STDERR.puts "Exception!\n#{except.message}\n"
        exit -1
      end

      fpl_file.printf("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n")
      fpl_file.printf("<ScnFlightPlanList>\n")

      @flp_data.each do |callsign, fpl|
        # Generate the bulk of the FPL
        fpl_file.printf("\t<ScnFlightPlan>\n")
        fpl_file.printf("\t\t<CallSign>%s</CallSign>\n", callsign)
        fpl_file.printf("\t\t<Iff3>%d</Iff3>\n", fpl[callsign][:plan].iff3)
        fpl_file.printf("\t\t<SerialNumber>1</SerialNumber>\n")
        fpl_file.printf("\t\t<Designator>%s</Designator>\n", callsign)
        fpl_file.printf("\t\t<Originator>%s</Originator>\n", fpl[callsign][:plan].originator)
        fpl_file.printf("\t\t<Rule>%s</Rule>\n", fpl[callsign][:plan].flrules)
        fpl_file.printf("\t\t<FlightType>%s</FlightType>\n", fpl[callsign][:plan].fltype)
        fpl_file.printf("\t\t<AircraftNumber>%d</AircraftNumber>\n", fpl[callsign][:plan].acnum)
        fpl_file.printf("\t\t<Turbulence>%s</Turbulence>\n", fpl[callsign][:plan].turbulence)
        fpl_file.printf("\t\t<Equipment>%s</Equipment>\n", fpl[callsign][:plan].equip)
        fpl_file.printf("\t\t<SSREquipment>%s</SSREquipment>\n", fpl[callsign][:plan].survequip)
        fpl_file.printf("\t\t<Eatd>%d</Eatd>\n", fpl[callsign][:plan].date)
        fpl_file.printf("\t\t<Eet>%d</Eet>\n", fpl[callsign][:plan].eet)
        fpl_file.printf("\t\t<InfoText>    </InfoText>\n")
        fpl_file.printf("\t\t<Adep>%s</Adep>\n", fpl[callsign][:plan].adep)
        fpl_file.printf("\t\t<Ades>%s</Ades>\n", fpl[callsign][:plan].ades)
        fpl_file.printf("\t\t<AltAirport1>%s</AltAirport1>\n", fpl[callsign][:plan].alt1)
        fpl_file.printf("\t\t<AltAirport2>%s</AltAirport2>\n", fpl[callsign][:plan].alt2)
        fpl_file.printf("\t\t<SimLevelParam>\n")
        fpl_file.printf("\t\t\t<LevelType>%s</LevelType>\n", fpl[callsign][:plan].rfl_type)
        fpl_file.printf("\t\t\t<LevelValue>%d</LevelValue>\n", fpl[callsign][:plan].rfl_val)
        fpl_file.printf("\t\t\t<LevelUnit>%s</LevelUnit>\n", fpl[callsign][:plan].rfl_unit)
        fpl_file.printf("\t\t</SimLevelParam>\n")
        fpl_file.printf("\t\t<SimSpeedParam>\n")
        fpl_file.printf("\t\t\t<SpeedType>%s</speed_type>\n", fpl[callsign][:plan].speed_type)
        fpl_file.printf("\t\t\t<SpeedValue>%d</speed_value>\n", fpl[callsign][:plan].speed_val)
        fpl_file.printf("\t\t\t<SpeedUnit>%s</SpeedUnit>\n", fpl[callsign][:plan].speed_unit)
        fpl_file.printf("\t\t</SimSpeedParam>\n")
        fpl_file.printf("\t\t<ScnFplWayPointList>\n")

        # Generate points report
        fpl[callsign][:position].each do |posi|
          if match_data = posi.match(/^([NS][0-9]{2}([0-9]{2})?([0-9]{2})?)([EW][0-9]{3}([0-9]{2})?([0-9]{2})?)$/)
            if match_data[1] && !match_data[2]
              lat = "#{match_data[0]}#{match_data[1]}00"
            elsif !match_data[1]
              lat = "#{match_data[0]}0000"
            else
              lat = "#{match_data[0]}#{match_data[1]}#{match_data[2]}"
            end

            if match_data[4] && !match_data[5]
              long = "#{match_data[3]}#{match_data[4]}00"
            elsif !match_data[1]
              long = "#{match_data[3]}0000"
            else
              long = "#{match_data[3]}#{match_data[4]}#{match_data[5]}"
            end

            fpl_file.printf("\t\t\t<ScnFplWayPoint>")
            fpl_file.printf("\t\t\t\t<Type>POINT</Type>")
            fpl_file.printf("\t\t\t\t<GeoPoint>")
            fpl_file.printf("\t\t\t\t\t<Latitude>%s</Latitude>", lat)
            fpl_file.printf("\t\t\t\t\t<Longitude>%s</Longitude>", long)
            fpl_file.printf("\t\t\t\t</GeoPoint>")
            fpl_file.printf("\t\t\t\t<DataIndic>NO</DataIndic>")
            fpl_file.printf("\t\t\t</ScnFplWayPoint>")
          end
        end

        fpl_file.printf("\t\t</ScnFplWayPointList>")
        fpl_file.printf("\t\t<ScnFplAircraftList>")
        fpl_file.printf("\t\t<ScnFplAircraft>")
        fpl_file.printf("\t\t\t<Icao>%s</Icao>", fpl[callsign][:plan].actype)
        fpl_file.printf("\t\t\t<Count>%d</Count>", fpl[callsign][:plan].acnum)
        fpl_file.printf("\t\t</ScnFplAircraft>")
        fpl_file.printf("\t\t</ScnFplAircraftList>")
        fpl_file.printf("\t\t<ScnMouvementList>")
        fpl_file.printf("\t\t</ScnMouvementList>")
        fpl_file.printf("\t</ScnFlightPlan>")
      end

      fpl_file.printf("</ScnFlightPlanList>\n")

      fpl_file.close
    end
  end
end
