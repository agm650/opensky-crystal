module TRS::Soprano
  struct AirPlatform
    getter callsign : String
    getter ac_type : String
    getter now_tstamp : Int64
    getter ssr_code : Int64
    getter icao24 : String

    def initialize(@callsign : String, @ac_type : String, now : Int64, @ssr_code : Int64, @icao24 : String)
      @now_tstamp = now * 1_000
    end
  end

  class Airptf
    property air_ptfs = [] of AirPlatform
    getter scenario_name = String

    def self.instance
      @@instance ||= new
    end

    def initialize(@scenario_name : String)
    end

    def add_air_ptf(ptf : AirPlatform)
      air_ptfs.push(ptf)
    end

    def generate_xml
      begin
        airptf_xml_file = File.open("#{@scenario_name}/ScnAirPlatform.xml", "w")
      rescue except
        STDERR.puts "Exception!\n#{except.message}\n"
        exit -1
      end
      airptf_xml_file.printf("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n<ScnAirplatformList>\n")

      air_ptfs.each do |ptf|
        airptf_xml_file.printf("\t<ScnAirPlatform>\n")
        airptf_xml_file.printf("\t\t<ScnPlatform>\n")
        airptf_xml_file.printf("\t\t\t<Name>#{ptf.callsign}</Name>\n")
        airptf_xml_file.printf("\t\t\t<Allegiance>OF</Allegiance>\n")
        airptf_xml_file.printf("\t\t\t<Live>NO</Live>\n")
        airptf_xml_file.printf("\t\t\t<Side>BLUE</Side>\n")
        airptf_xml_file.printf("\t\t\t<Strength>100</Strength>\n")
        airptf_xml_file.printf("\t\t</ScnPlatform>\n")
        airptf_xml_file.printf("\t\t<Unit></Unit>\n")
        airptf_xml_file.printf("\t\t<ApfType>#{ptf.ac_type}</ApfType>\n")
        airptf_xml_file.printf("\t\t<MovementsList>\n")
        airptf_xml_file.printf("\t\t\t<ScnSpecificMvmtInfo>\n")
        airptf_xml_file.printf("\t\t\t\t<Position>1</Position>\n")
        airptf_xml_file.printf("\t\t\t\t<CommonMvmtInfo>air@#{ptf.callsign}</CommonMvmtInfo>\n")
        airptf_xml_file.printf("\t\t\t\t<ScnMvmtInfo>\n")
        airptf_xml_file.printf("\t\t\t\t\t<ScnPlatformConfig>\n")
        airptf_xml_file.printf("\t\t\t\t\t\t<FuelQuantity>0.0</FuelQuantity>\n")
        airptf_xml_file.printf("\t\t\t\t\t\t<L16Equipment>NO</L16Equipment>\n")
        airptf_xml_file.printf("\t\t\t\t\t</ScnPlatformConfig>\n")
        if ptf.now_tstamp
          airptf_xml_file.printf("\t\t\t\t\t<SimTemporalEntityEventList>\n")
          if ptf.ssr_code
            airptf_xml_file.printf("\t\t\t\t\t\t<SimTemporalEntityEvent>\n")
            airptf_xml_file.printf("\t\t\t\t\t\t\t<ScnTemporalEvent>\n")
            airptf_xml_file.printf("\t\t\t\t\t\t\t\t<Begin>#{ptf.now_tstamp}</Begin>\n")
            airptf_xml_file.printf("\t\t\t\t\t\t\t</ScnTemporalEvent>\n")
            airptf_xml_file.printf("\t\t\t\t\t\t\t<ScnIffEvent>\n")
            airptf_xml_file.printf("\t\t\t\t\t\t\t\t<Mode>3</Mode>\n")
            airptf_xml_file.printf("\t\t\t\t\t\t\t\t<Value>%04o</Value>\n", ptf.ssr_code)
            airptf_xml_file.printf("\t\t\t\t\t\t\t\t<Emitting>ON</Emitting>\n")
            airptf_xml_file.printf("\t\t\t\t\t\t\t</ScnIffEvent>\n")
            airptf_xml_file.printf("\t\t\t\t\t\t</SimTemporalEntityEvent>\n")
          end
          airptf_xml_file.printf("\t\t\t\t\t\t<SimTemporalEntityEvent>\n")
          airptf_xml_file.printf("\t\t\t\t\t\t\t<ScnTemporalEvent>\n")
          airptf_xml_file.printf("\t\t\t\t\t\t\t\t<Begin>#{ptf.now_tstamp}</Begin>\n")
          airptf_xml_file.printf("\t\t\t\t\t\t\t</ScnTemporalEvent>\n")
          airptf_xml_file.printf("\t\t\t\t\t\t\t<ScnIffEvent>\n")
          airptf_xml_file.printf("\t\t\t\t\t\t\t\t<Mode>C</Mode>\n")
          airptf_xml_file.printf("\t\t\t\t\t\t\t\t<Emitting>ON</Emitting>\n")
          airptf_xml_file.printf("\t\t\t\t\t\t\t</ScnIffEvent>\n")
          airptf_xml_file.printf("\t\t\t\t\t\t</SimTemporalEntityEvent>\n")

          if ptf.icao24.size > 0
            airptf_xml_file.printf("\t\t\t\t\t\t<SimTemporalEntityEvent>\n")
            airptf_xml_file.printf("\t\t\t\t\t\t\t<ScnTemporalEvent>\n")
            airptf_xml_file.printf("\t\t\t\t\t\t\t\t<Begin>#{ptf.now_tstamp}</Begin>\n")
            airptf_xml_file.printf("\t\t\t\t\t\t\t</ScnTemporalEvent>\n")
            airptf_xml_file.printf("\t\t\t\t\t\t\t<ScnIffEvent>\n")
            airptf_xml_file.printf("\t\t\t\t\t\t\t\t<Mode>S</Mode>\n")
            airptf_xml_file.printf("\t\t\t\t\t\t\t\t<Adress>%06x</Adress>\n", ptf.icao24.to_i(16))
            airptf_xml_file.printf("\t\t\t\t\t\t\t\t<AircraftId>#{ptf.callsign}</AircraftId>\n")
            airptf_xml_file.printf("\t\t\t\t\t\t\t\t<Emitting>ON</Emitting>\n")
            airptf_xml_file.printf("\t\t\t\t\t\t\t</ScnIffEvent>\n")
            airptf_xml_file.printf("\t\t\t\t\t\t</SimTemporalEntityEvent>\n")
          end

          airptf_xml_file.printf("\t\t\t\t\t</SimTemporalEntityEventList>\n")
        end

        airptf_xml_file.printf("\t\t\t\t</ScnMvmtInfo>\n")
        airptf_xml_file.printf("\t\t\t</ScnSpecificMvmtInfo>\n")
        airptf_xml_file.printf("\t\t</MovementsList>\n")
        airptf_xml_file.printf("\t</ScnAirPlatform>\n")
      end

      airptf_xml_file.printf("</ScnAirplatformList>\n\r")
      airptf_xml_file.close
    end
  end
end
