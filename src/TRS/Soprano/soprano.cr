module TRS::Soprano
  def generate_borders(scn_name : String)
    begin
      border_file = File.open("#{scn_name}/#{scn_name}.borders_lst", "w")
    rescue except
      STDERR.puts "Exception!\n#{except.message}\n"
      exit -1
    end
    border_file.printf("fr.txt\t\t1 2 3 0 0\nan.txt\t\t1 1 3 0 0\nls.txt\t\t1 1 3 0 0\nbe.txt\t\t1 1 3 0 0\n")
    border_file.printf("au.txt\t\t1 1 3 0 0\nlu.txt\t\t1 1 3 0 0\npo.txt\t\t1 1 3 0 0\nsp.txt\t\t1 1 3 0 0\n")
    border_file.printf("it.txt\t\t1 1 3 0 0\nnl.txt\t\t1 1 3 0 0\nuk.txt\t\t1 1 3 0 0\ngm.txt\t\t1 1 3 0 0\n")
    border_file.printf("sz.txt\t\t1 1 3 0 0\n")
    border_file.close
  end

  def generate_land(scn_name : String)
    begin
      land_file = File.open("#{scn_name}/#{scn_name}.land_lst", "w")
    rescue except
      STDERR.puts "Exception!\n#{except.message}\n"
      exit -1
    end
    land_file.printf("fr.txt\t\t1 2 3 0 0\nan.txt\t\t1 1 3 0 0\nls.txt\t\t1 1 3 0 0\nbe.txt\t\t1 1 3 0 0\n")
    land_file.printf("au.txt\t\t1 1 3 0 0\nlu.txt\t\t1 1 3 0 0\npo.txt\t\t1 1 3 0 0\nsp.txt\t\t1 1 3 0 0\n")
    land_file.printf("it.txt\t\t1 1 3 0 0\nnl.txt\t\t1 1 3 0 0\nuk.txt\t\t1 1 3 0 0\ngm.txt\t\t1 1 3 0 0\n")
    land_file.printf("sz.txt\t\t1 1 3 0 0\n")
    land_file.close
  end

  def generate_summary(scn_name : String, cpt_air_ptf : Int = 0)
    begin
      summary_file = File.open("#{scn_name}/summary.xml", "w")
    rescue except
      STDERR.puts "Exception!\n#{except.message}\n"
      exit -1
    end

    summary_file.printf("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>
<ScnSummaryList>
	<ScnSummary>
		<Duration>43200000</Duration>
		<Name>#{scn_name}.scn</Name>
		<CenterList>
		</CenterList>
		<SensorList>
		</SensorList>
		<NbMobileAir>#{cpt_air_ptf}</NbMobileAir>
		<NbMobileSea>0</NbMobileSea>
		<NbMobileLand>0</NbMobileLand>
		<NbJammer>0</NbJammer>
		<NbEmitter>0</NbEmitter>
		<NbFligthPlan>0</NbFligthPlan>
		<NbL1France>0</NbL1France>
		<NbL1OTAN>0</NbL1OTAN>
		<NbL16>0</NbL16>
		<NbL11A>0</NbL11A>
		<NbL11B>0</NbL11B>
		<NbLRECCI>0</NbLRECCI>
		<NbLCAUTRAtrk>0</NbLCAUTRAtrk>
		<NbLCAUTRAfpl>0</NbLCAUTRAfpl>
		<NbLMidsISARD>0</NbLMidsISARD>
		<NbL11ISARD>0</NbL11ISARD>
		<NbLICCT>0</NbLICCT>
	</ScnSummary>
</ScnSummaryList>
\r")
    summary_file.close
  end

  def generate_version(scn_name : String)
    begin
      version_file = File.open("#{scn_name}/version.xml", "w")
    rescue except
      STDERR.puts "Exception!\n#{except.message}\n"
      exit -1
    end

    version_file.printf("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>
<Version>
	<SopranoVersion>2.0</SopranoVersion>
	<AffairName>Test Environnement LOC1</AffairName>
	<AffairVersion>2.69.0</AffairVersion>
	<PoseidonVersion>CDEVS_004_massy_20030602</PoseidonVersion>
</Version>\n\r")

    version_file.close
  end
end
