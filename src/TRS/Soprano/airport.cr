module TRS::Soprano
  struct Airstrip
    def initialize(@ab_name : String, @latitude : Float64, @longitude : Float64, @elevation : Float64)
    end
  end

  class Airport
    property airport_data = {} of String => Airstrip

    def self.instance
      @@instance ||= new
    end

    def initialize
      if !@airport_data
        @@irport_data = Hash(String, Airstrip).new
      end
    end

    def add_airport(icao : String, ab_name : String, latitude : Float64, longitude : Float64, elevation : Float64)
      @@airport_data[icao] = Airstrip.new(ab_name, latitude, longitude, elevation)
    end

    def generate_xml(scenario_name : String)
      begin
        scen_file = File.open(scenario_name + "/ScnAirport.xml", "w")
      rescue except
        STDERR.puts "Exception!\n#{except.message}\n"
        exit -1
      end
      scen_file.printf("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n<ScnAirportList>\n")

      airport_data.each do |key, value|
        scen_file.printf("\t<ScnAirport>\n")
        scen_file.printf("\t\t<Icao>%s</Icao>\n", key)
        scen_file.printf("\t\t<Name>%s</Name>\n", value[key].ab_name)
        scen_file.printf("\t\t<Alias>%s</Alias>\n", key)
        scen_file.printf("\t\t<Ata>UNK</Alias>\n")
        scen_file.printf("\t\t<AirportType>AIRFIELD</AirportType>\n")
        scen_file.printf("\t\t<GeoPoint>\n")
        scen_file.printf("\t\t\t<Latitude>%f</Latitude>\n", value[key].latitude)
        scen_file.printf("\t\t\t<Longitude>%f</Longitude>\n", value[key].longitude)
        scen_file.printf("\t\t</GeoPoint>\n")
        scen_file.printf("\t\t<Elevation>%f</Elevation>\n", value[key].elevation)
        scen_file.printf("\t</ScnAirport>\n")
      end
      scen_file.printf("</ScnAirportList>\n\r")
      scen_file.close
    end
  end
end
