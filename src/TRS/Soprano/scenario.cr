module TRS::Soprano
  class TRS::Soprano::Scenario
    property orbit_list = [] of String
    property fpl_list = [] of NamedTuple(callsign: String, adep: String, ades: String)
    property aptf_list = {} of String => String
    property profile_list = [] of NamedTuple(callsign: String, ac_type: String, init_pt: String, ades: String, orbits: Array(String))

    def self.instance
      @@instance ||= new
    end

    def add_profile(callsign : String, ac_type : String, init_pt : String, ades : String, orbits : Array(String) = [] of String)
      profile_list.push({callsign: callsign, ac_type: ac_type, init_pt: init_pt, ades: ades, orbits: orbits})
    end

    def add_aptf(callsign : String, ac_type : String)
      aptf_list[callsign] = ac_type
    end

    def add_fpl(callsign : String, adep : String, ades : String)
      fpl_list.push[{callsign: callsign, adep: adep, ades: ades}]
    end

    def add_orbit(orb_name : String)
      orbit_list.push[orb_name]
    end

    def generate_file(scn_name : String, date : Time)
      begin
        scn_file = File.open("#{scn_name}.scn", "w")
      rescue except
        STDERR.puts "Exception!\n#{except.message}\n"
        exit -1
      end

      scn_file.printf("%02d/%02d/%04d-%02d:%02d:%02d\r\nlimited resources\r\n",
        date.day,
        date.month,
        date.year,
        date.hour,
        date.minute,
        date.second)

      scn_file.printf("DIREX\r\nlst\r\n")
      scn_file.printf("46.0 2.0 0.0\r\n")
      scn_file.printf("Borders\t#{scn_name}\\#{scn_name}.borders_lst\r\n")
      scn_file.printf("Land\t#{scn_name}\\#{scn_name}.land_lst\r\n")
      # scn_file.printf("Bases\t#{scn_name}\\ScnAirport.xml\r\n")

      if orbit_list.size > 0
        scn_file.printf("Orbits\t#{scn_name}\\ScnOrbit.xml\r\n")
      end

      if aptf_list.size > 0
        scn_file.printf("profile\t#{scn_name}\\ScnAirPlatform.xml\r\n")
      end

      if fpl_list.size > 0
        scn_file.printf("FlightPlan\t#{scn_name}\\ScnFlightPlan.xml\r\n")
      end

      scn_file.puts
      scn_file.puts
      scn_file.printf("START_XML\n<Node>\n\t<Label>#{scn_name}.scn</Label>\n")
      scn_file.printf("\t<Children>\n")
      scn_file.printf("\t\t<Node>\n\t\t\t<Manager>Land</Manager>\n\t\t\t<IsVisible>YES</IsVisible>\n\t\t</Node>\n")
      scn_file.printf("\t\t<Node>\n\t\t\t<Manager>Bases</Manager>\n\t\t\t</Node>\n")
      scn_file.printf("\t\t<Node>\n\t\t\t<Manager>Borders</Manager>\n\t\t\t<IsVisible>YES</IsVisible>\n\t\t</Node>\n")

      if orbit_list.size > 0
        scn_file.printf("\t\t<Node>\n\t\t\t<Manager>Orbits</Manager>\n\t\t\t<IsVisible>YES</IsVisible>\n\t\t\t<Children>\n")
        orbit_list.each do |orb_name|
          scn_file.printf("\t\t\t\t<Node>\n\t\t\t\t\t<Label>#{orb_name}</Label>\n\t\t\t\t\t<Key>#{orb_name}</Key>\n")
          scn_file.printf("\t\t\t\t\t<Manage>Orbits</Manage>\n\t\t\t\t\t<IsVisible>YES</IsVisible>\n\t\t\t\t</Node>\n")
        end
        scn_file.printf("\t\t\t</Children>\n\t\t</Node>\n")
      end

      if aptf_list.size > 0
        scn_file.printf("\t\t<Node>\n\t\t\t<Manager>AirPlatform</Manager>\n\t\t\t<Children>\n")
        aptf_list.each do |callsign, ac_type|
          scn_file.printf("\t\t\t\t<Node>\n")
          scn_file.printf("\t\t\t\t\t<Label>#{callsign} ( #{ac_type} )</Label>\n")
          scn_file.printf("\t\t\t\t\t<Key>#{callsign}</Key>\n")
          scn_file.printf("\t\t\t\t\t<Manager>AirPlatform</Manager>\n")
          scn_file.printf("\t\t\t\t\t<IsVisible>YES</IsVisible>\n")
          scn_file.printf("\t\t\t\t</Node>\n")
        end
        scn_file.printf("\t\t\t</Children>\n\t\t</Node>\n")
      end

      if fpl_list.size > 0
        scn_file.printf("\t\t<Node>\n\t\t\t<Manager>FlightPlan</Manager>\n\t\t\t<Children>\n")
        fpl_list.each do |fpl|
          scn_file.printf("\t\t\t\t<Node>\n")
          scn_file.printf("\t\t\t\t\t<Label>#{fpl[:callsign]} ( #{fpl[:adep]} -> #{fpl[:ades]} )</Label>\n")
          scn_file.printf("\t\t\t\t\t<Key>#{fpl[:callsign]}</Key>\n")
          scn_file.printf("\t\t\t\t\t<Manager>AirPlatform</Manager>\n")
          scn_file.printf("\t\t\t\t\t<IsVisible>YES</IsVisible>\n")
          scn_file.printf("\t\t\t\t</Node>\n")
        end
        scn_file.printf("\t\t\t</Children>\n\t\t</Node>\n")
      end

      if profile_list.size > 0
        scn_file.printf("\t\t<Node>\n\t\t\t<Manager>profile</Manager>\n\t\t\t<IsVisible>YES</IsVisible>\n\t\t\t<Children>\n")

        profile_list.each do |profile|
          scn_file.printf("\t\t\t\t<Node>\n")
          scn_file.printf("\t\t\t\t\t<Label>#{profile[:callsign]} ( 1 #{profile[:ac_type]} : #{profile[:init_pt]} -> #{profile[:ades]} )</Label>\n")
          scn_file.printf("\t\t\t\t\t<Key>#{profile[:callsign]}</Key>\n")
          scn_file.printf("\t\t\t\t\t<Manager>profile</Manager>\n")
          scn_file.printf("\t\t\t\t\t<IsVisible>YES</IsVisible>\n")
          scn_file.printf("\t\t\t\t\t<Children>\n")
          scn_file.printf("\t\t\t\t\t\t<Link>\n")
          scn_file.printf("\t\t\t\t\t\t\t<Key>#{profile[:callsign]}</Key>\n")
          scn_file.printf("\t\t\t\t\t\t\t<Manager>AirPlatform</Manager>\n")
          scn_file.printf("\t\t\t\t\t\t\t<IsVisible>YES</IsVisible>\n")
          scn_file.printf("\t\t\t\t\t\t</Link>\n")

          profile[:orbits].each do |orb_name|
            scn_file.printf("\t\t\t\t\t\t<Link>\n")
            scn_file.printf("\t\t\t\t\t\t\t<Key>#{orb_name}</Key>\n")
            scn_file.printf("\t\t\t\t\t\t\t<Manager>Orbits</Manager>\n")
            scn_file.printf("\t\t\t\t\t\t\t<IsVisible>YES</IsVisible>\n")
            scn_file.printf("\t\t\t\t\t\t</Link>\n")
          end

          scn_file.printf("\t\t\t\t\t</Children>\n")

          scn_file.printf("\t\t\t\t</Node>\n")
        end
        scn_file.printf("\t\t\t</Children>\n\t\t</Node>\n")
      end
      scn_file.printf("\t</Children>\n</Node>\n")
      scn_file.close
    end
  end
end
