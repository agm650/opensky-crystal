require "math"
require "sqlite3"

module TRS
  class TRS::FlightPlan
    getter raw : String

    processed_message : String
    fields = {} of Int32 | String => String

    def initialize(@raw : String)
    end

    # Class methods
    def TRS::FlightPlan.gen_ssr_code(avoid_special : Bool = true)
      dec_ssr_code : Int16 = Random.rand(4096).to_i16
      if avoid_special
        while dec_ssr_code == 0o7500 || dec_ssr_code == 0o7600 || dec_ssr_code == 0o7700
          puts "Looping #{dec_ssr_code}"
          dec_ssr_code = Random.rand(4096).to_i16
        end
      end
      sprintf("%04o", dec_ssr_code)
    end

    def TRS::FlightPlan.replace_geo_pt
      # TODO
      result_route : String = ""
    end

    # Extraction et mise dans l'ordre de passage des points.
    # Attention pas de traduction eventuelle des "GEOXX"
    def TRS::FlightPlan.extract_rtepts(rtpts : String)
      working_string = rtpts.strip

      pts_list = working_string.split(/-PT\s+/)
      # Removing empty items
      pts_list.delete("")
      route_points = {} of Int32 => NamedTuple(point: String, eto: String, level: String)

      cas_tordu = false
      md = working_string.match(/-TO\s*23[0-9]{2}/i)
      md1 = working_string.match(/-TO\s*00[0-9]{2}/i)
      if md && md1
        # print STDERR "Cas tordu...\n";
        cas_tordu = true
      end

      pts_list.each do |my_pt|
        my_pt_wip = my_pt.strip

        # Message au format ADEXP pur
        if md = my_pt_wip.match(/-PTID\s+([[:alnum:]]{2,5})\s+-TO\s+([[:digit:]]{4})\s*(?:-FL\s+((?:[FA][[:digit:]]{3})|(?:[SM][[:digit:]]{4})))?/xi)
          # logiquement on ne devrait pas pouvoir etre a deux endroits en meme temps, donc on se sert de l'heure comme clé
          # print STDERR "\t Result: " . $1 . " | ".$2." | ".$3."\n";
          key_hash_sort = md[2].to_i
          eto = md[2]
          point_match = md[1]
          level_match = "0"
          if md[3]?
            level_match = md[3]
          end
          # Cas tordu, la gestion des plan de vol a cheval sur deux jours...
          # a ameliorer...

          if cas_tordu && key_hash_sort >= 0 && key_hash_sort < 1200
            key_hash_sort = 10_000 + key_hash_sort
          end

          if !route_points[key_hash_sort]?
            route_points[key_hash_sort] = {point: point_match, eto: eto, level: level_match}
          end
        end
      end

      result_array = [] of NamedTuple(point: String, eto: String, level: String)
      result_key = route_points.keys.sort { |a, b| a <=> b }

      result_key.each do |key|
        result_array << route_points[key]
      end
      result_array
    end

    # Instances methods
    def extract_field_from_message(db : Database)
      # The db is already opened, just need to use it.
      raw_message = self.raw
      # Removing first/last parenthesis if present
      raw_message = raw_message.gsub(/^\s*\(/, "")
      raw_message = raw_message.gsub(/\)\s*$/, "")

      seqnum : UInt16
      ats_sending : String
      ats_recving : String
      seqnum_ref : UInt16
      ats_sending_ref : String
      ats_recving_ref : String

      data : Array(String) = raw_message.split("-")

      if data[0].match(/CPL/xi)
        # On a un CPL => ICAO
        fields = extract_field_from_icao_message(data)
      elsif data[0].match(/FPL/xi)
        # On a un FPL => ICAO
        fields = extract_field_from_icao_message(data)
      elsif data[1].match(/TITLE(?:\s*(IFPL|BFD))/xi)
        fields = extract_field_from_adexp_message(data, raw_message)
      end
      fields
    end

    def extract_field_from_adexp_message(data : Array(String), message : String)
      tmp_fields = {} of Int32 | String => String

      seqnum : UInt16
      ats_sending : String
      ats_recving : String
      seqnum_ref : UInt16
      ats_sending_ref : String
      ats_recving_ref : String

      if md = data[1].match(/TITLE(?:\s*(IFPL|BFD))/xi)
        if md = message.match(/-REFDATA\s+-SENDER\s+-FAC\s+([[:alpha:]]{1,4})\s+-RECVR\s+-FAC\s+([[:alpha:]]{1,4})\s+-SEQNUM\s+([[:digit:]]{1,3})/ix)
          ats_sending = md[1]
          ats_recving = md[2]
          seqnum = md[3]
        end

        idx_first_known_point : UInt16 = 0
        first_known_point : String

        tmp_fields["type"] = md[1]

        tmp_fields["seqnum"] = seqnum
        if seqnum?
          tmp_fields[3] = "#{ats_sending}/#{ats_recving}#{seqnum}"
        end
        if md = message.match(/-MSGREF\s+-SENDER\s+-FAC\s+([[:alpha:]]{1,4})\s+-RECVR\s+-FAC\s+([[:alpha:]]{1,4})\s+-SEQNUM\s+([[:digit:]]{1,3})/ix)
          ats_sending_ref = md[1]
          ats_recving_ref = md[2]
          seqnum_ref = md[3]
          tmp_fields[3] = tmp_fields[3] + ats_sending_ref + "/" + ats_recving_ref + seqnum_ref
        end

        if md = message.match(/-ARCID\s+([[:alnum:]]+)/xi)
          tmp_fields[7] = md[1]
        end

        if md = message.match(/-SSRCODE\s*(A[0-7]{4})/xi)
          tmp_fields[7] = "#{tmp_fields[7]}/#{md[1]}"
        end

        flight_rules : String
        flight_type : String

        if md = message.match(/-FLTRUL\s+([IVYZ])/xi)
          flight_rules = md[1]
        else
          flight_rules = "I"
        end

        if md = message.match(/-FLTTYP\s+([SNGMX]+)/xi)
          flight_type = md[1]
        else
          flight_type = "X"
        end

        tmp_fields[8] = "#{flight_rules}#{flight_type}"

        # AC Type, AC Number, Wake Turbulence
        tmp_fields[9] = extract_adexp_field9(message)

        # CEQPT / SEQPT
        tmp_fields[10] = extract_adexp_field10(message)

        # ADEP
        tmp_fields[13] = extract_adexp_adep(message)

        rtepts_field = ""
        if md = message.match(/-BEGIN\s+RTEPTS\s+([[:ascii:]]+)-END\s+RTEPTS/xi)
          rtepts_field = md[1]
          tmp_fields["RTEPTS"] = extract_rtepts(rtepts_field)
        end

        # if tmp_fields["type"] =~ /BFD/
        #   route_points : Array(String)
        #   if rtepts_field?
        #     route_points = extract_rtepts(rtepts_field)
        #   end
        # end
      end
      tmp_fields
    end

    def extract_adexp_field9(message : String) : String
      field_9 : String = ""
      if md = message.match(/-ARCTYP\s+([[:alnum:]]+)/xi)
        field_9 = md[1]
        if md = message.match(/-WKTRC\s+([LMHJ]+)/xi)
          field_9 = "#{field_9}/#{md[1]}"
        else
          # Wake Turb missing, using M as default value
          field_9 = "#{field_9}/M"
        end
      end

      if md = message.match(/-NBARC\s+([[:digit:]]+)/xi)
        field_9 = "#{md[1]}#{field_9}"
      end

      field_9
    end

    def extract_adexp_field10(message : String) : String
      field_10 : String = ""
      if md = message.match(/-CEQPT\s+([[:alpha:]]+)/xi)
        field_10 = md[1]
        if md = message.match(/-SEQPT\s+([NACXPIS]{1}[D]{0,1})/xi)
          field_10 = "#{tmp_fields[10]}/#{md[1]}"
        end
      end
      field_10
    end

    def extract_adexp_adep(message : String) : String
      adep : String = ""
      # EOBT
      if md = message.match(/-ADEP\s+([[:alpha:]]{4})/xi)
        adep = md[1]
        if md = message.match(/-EOBT\s+([[:digit:]]{4})/xi)
          adep = "#{adep}/#{md[1]}"
        end
      end
      adep
    end

    def extract_field_from_icao_message(data : Array(String))
      tmp_fields = {} of Int32 | String => String
      if md = data[0].match(/CPL/xi)
        tmp_fields["type"] = "CPL"
        # data[0] => Field 3
        if md = data[0].match(/^CPL([[:alpha:]]{1,4}\/[[:alpha:]]{1,4}[[:digit:]]{1,3})([[:alpha:]]{1,4}\/[[:alpha:]]{1,4}[[:digit:]]{1,3})?/xi)
          tmp_fields[3] = md[1]
          tmp_fields[3] = tmp_fields[3] + md[2] if md[2]?
        else
          tmp_fields[3] = ""
        end
        tmp_fields["seqnum"] = tmp_fields[3]

        # data[1] => Field 7
        tmp_fields[7] = data[1]?
        # data[2] => Field 8
        tmp_fields[8] = data[2]?
        # data[3] => Field 9
        tmp_fields[9] = data[3]?
        # data[4] => Field 10
        tmp_fields[10] = data[4]?
        # data[5] => Field 13
        tmp_fields[13] = data[5]?
        # data[6] => Field 14
        tmp_fields[14] = data[6]?
        # data[7] => Field 15
        tmp_fields[15] = data[7]?
        # data[8] => Field 16
        tmp_fields[16] = data[8]?
        # data[9] => Field 18
        tmp_fields[18] = data[9]?
      elsif md = data[0].match(/FPL/xi)
        # On a un FPL
        tmp_fields["type"] = "FPL"
        # la structure d'un CPL est la suivante:
        # data[0] => Field 3
        if md = data[0].match(/^FPL([[:alpha:]]{1,4}\/[[:alpha:]]{1,4}[[:digit:]]{1,3})([[:alpha:]]{1,4}\/[[:alpha:]]{1,4}[[:digit:]]{1,3})?/xi)
          tmp_fields[3] = md[1]
          if md[2]?
            tmp_fields[3] = tmp_fields[3] + md[2]
          end
        else
          tmp_fields[3] = ""
        end
        tmp_fields["seqnum"] = tmp_fields[3]

        # data[1] => Field 7
        tmp_fields[7] = data[1]
        # data[2] => Field 8
        tmp_fields[8] = data[2]
        # data[3] => Field 9
        tmp_fields[9] = data[3]
        # data[4] => Field 10
        tmp_fields[10] = data[4]
        # data[5] => Field 13
        tmp_fields[13] = data[5]
        # data[6] => Field 15
        tmp_fields[15] = data[6]
        # data[7] => Field 16
        tmp_fields[16] = data[7]
        # data[8] => Field 18
        tmp_fields[18] = data[8]
      else
        raise Exception.new("Invalid or unsupported ICAO message")
      end
      tmp_fields
    end
  end
end
