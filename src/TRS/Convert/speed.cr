module TRS::Convert
  class TRS::Convert::Speed
    property speed : (Float64)?
    property unit : (String)?

    def initialize(@speed : Float64, @unit : String)
    end

    def initialize(raw_string : String)
      if md = raw_string.match(/^\s*(?:([NK])([0-9]{4}))|(?:(M)([0-9]{3}))\s*$/)
        self.speed = md[1].to_f
        self.unit = md[2].to_f
      else
        Exception.new("Argument is not a valid speed definition")
      end
    end

    def convert_to_knots
      case unit
      when "K"
        speed / 1.852
      when "N"
        speed
      when "M"
        speed * 666.739
      else
      end
    end
  end
end
