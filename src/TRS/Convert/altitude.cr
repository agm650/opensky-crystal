module TRS::Convert
  class TRS::Convert::Altitude
    property unit : (String)?
    property value : (Float64)?

    def initialize(lunit : String, lvalue : Float64)
      @unit = lunit
      @value = lvalue
    end

    def initialize(raw_string : String)
      if md = raw_string.match(/^\s*(?:([AF])([0-9]{3}))|(?:([SM])([0-9]{4}))\s*$/)
        self.speed = md[1].to_f
        self.unit = md[2].to_f
      else
        Exception.new("Argument is not a valid speed definition")
      end
    end

    def get_soprano_unit
      case @unit
      when "A", "F"
        "HFT"
      when "M", "S"
        "M"
      else
        "HFT"
      end
    end

    def get_soprano_alt
      case @unit
      when "A", "F", "M", "S"
        "ALTI"
      else
        "ALTI"
      end
    end

    def convert_to_hft
      case @unit
      when "A", "F"
        @value
      when "M", "S"
        @value * 3.281 / 10
      else
        @value
      end
    end

    def convert_to_meters
      case @unit
      when "A", "F"
        @value * 100 / 3.281
      when "M", "S"
        @value * 10
      else
        @value * 100 / 3.281
      end
    end
  end
end
