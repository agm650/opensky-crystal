require "math"

module TRS::Convert
  class TRS::Convert::Position
    include Math
    getter latitude : Float64 = 0
    getter longitude : Float64 = 0

    def initialize(raw_position : String)
      la_ori : Float64 = 1.0
      la_deg : Float64 = 0
      la_min : Float64 = 0
      la_sec : Float64 = 0

      lo_ori : Float64 = 1.0
      lo_deg : Float64 = 0
      lo_min : Float64 = 0
      lo_sec : Float64 = 0

      if md = raw_position.match(/^([NS])([0-9]{2})([0-9]{2})?([0-9]{2})?([EWO])([0-9]{3})([0-9]{2})?([0-9]{2})?$/)
        la_ori, la_deg, la_min, la_sec = _extract_lattitude_prefix(md)
        lo_ori, lo_deg, lo_min, lo_sec = _extract_longitude_prefix(md)
      elsif md = raw_position.match(/^([0-9]{2})([0-9]{2})?([0-9]{2})?([NS])([0-9]{3})([0-9]{2})?([0-9]{2})?([EWO])$/)
        la_ori, la_deg, la_min, la_sec = _extract_lattitude_postfix(md)
        lo_ori, lo_deg, lo_min, lo_sec = _extract_longitude_postfix(md)
      else
        raise Exception.new("Argument is not a valid position definition")
      end

      tmp_latitude = (la_deg + la_min + la_sec) * la_ori
      tmp_longitude = (lo_deg + lo_min + lo_sec) * lo_ori

      if tmp_latitude < 90.0 && tmp_latitude > -90.0 && tmp_longitude < 180.0 && tmp_longitude > -180.0
        @latitude = tmp_latitude
        @longitude = tmp_longitude
      else
        raise Exception.new("Invalid lat/long parameters")
      end
    end

    def initialize(dec_lat : Float64, dec_long : Float64)
      if dec_lat < 90.0 && dec_lat > -90.0 && dec_long < 180.0 && dec_long > -180.0
        @latitude = dec_lat
        @longitude = dec_long
      else
        raise Exception.new("Invalid lat/long parameters")
      end
    end

    def _extract_lattitude_prefix(matchdata : Regex::MatchData?) : Tupple(Float64, Float64, Float64, Float64)
      if matchdata
        la_ori : Float64 = -1.0 if md[1] == "S"
        la_deg : Float64 = md[2].to_f
        la_min : Float64 = (md[3].to_f / 0.6) / 100 if md[3]?
        la_sec : Float64 = (md[4].to_f / 0.6) / 10_000 if md[4]?
        return {la_ori, la_deg, la_min, la_sec}
      else
        raise Exception.new("Invalid lat parameters")
      end
    end

    def _extract_lattitude_postfix(matchdata : Regex::MatchData?) : Tupple(Float64, Float64, Float64, Float64)
      if matchdata
        la_ori : Float64 = -1.0 if md[4] == "S"
        la_deg : Float64 = md[1].to_f
        la_min : Float64 = (md[2].to_f / 0.6) / 100 if md[2]?
        la_sec : Float64 = (md[3].to_f / 0.6) / 10_000 if md[3]?
        return {la_ori, la_deg, la_min, la_sec}
      else
        raise Exception.new("Invalid lat parameters")
      end
    end

    def _extract_longitude_prefix(matchdata : Regex::MatchData?) : Tupple(Float64, Float64, Float64, Float64)
      if matchdata
        lo_ori : Float64 = -1.0 if md[5] == "W" || md[5] == "O"
        lo_deg : Float64 = md[6].to_f
        lo_min : Float64 = (md[7].to_f / 0.6) / 100 if md[7]?
        lo_sec : Float64 = (md[8].to_f / 0.6) / 10_000 if md[8]?
        return {lo_ori, lo_deg, lo_min, lo_sec}
      else
        raise Exception.new("Invalid long parameters")
      end
    end

    def _extract_longitude_postfix(matchdata : Regex::MatchData?) : Tupple(Float64, Float64, Float64, Float64)
      if matchdata
        lo_ori : Float64 = -1.0 if md[8] == "W" || md[8] == "O"
        lo_deg : Float64 = md[5].to_f
        lo_min : Float64 = (md[6].to_f / 0.6) / 100 if md[6]?
        lo_sec : Float64 = (md[7].to_f / 0.6) / 10_000 if md[7]?
        return {lo_ori, lo_deg, lo_min, lo_sec}
      else
        raise Exception.new("Invalid long parameters")
      end
    end

    def get_dms_latitude
      result_latitude : String = ""
      if latitude < 0
        result_latitude = "S"
      else
        result_latitude = "N"
      end
      lat = latitude.abs
      result_latitude = "#{result_latitude}" + sprintf("%02d", lat.to_i)
      dec_min = (lat - lat.to_i.to_f) * 100 * 0.6
      result_latitude = "#{result_latitude}" + sprintf "%02d", dec_min.to_i
      dec_sec = (dec_min - dec_min.to_i.to_f) * 100 * 0.6

      sprintf "%s%02d", result_latitude, dec_sec.to_i
    end

    def get_dms_longitude
      result_longitude : String = ""
      if longitude < 0
        result_longitude = "W"
      else
        result_longitude = "E"
      end
      long = longitude.abs
      result_longitude = "#{result_longitude}" + sprintf "%03d", long.to_i
      dec_min = (long - long.to_i.to_f) * 100 * 0.6
      result_longitude = "#{result_longitude}" + sprintf "%02d", dec_min.to_i
      dec_sec = (dec_min - dec_min.to_i.to_f) * 100 * 0.6

      sprintf "%s%02d", result_longitude, dec_sec.to_i
    end

    def get_soprano_latitude
      result_latitude : String = ""
      if latitude < 0
        result_latitude = "S "
      else
        result_latitude = "N "
      end
      lat = latitude.abs
      result_latitude = sprintf("%s%02d° ", result_latitude, lat.to_i)
      dec_min = (lat - lat.to_i.to_f) * 100 * 0.6
      result_latitude = sprintf "%s%02d' ", result_latitude, dec_min.to_i
      dec_sec = (dec_min - dec_min.to_i.to_f) * 100 * 0.6

      sprintf "%s%02d''", result_latitude, dec_sec.to_i
    end

    def get_soprano_longitude
      result_longitude : String = ""
      if longitude < 0
        result_longitude = "W "
      else
        result_longitude = "E "
      end
      long = longitude.abs
      result_longitude = "#{result_longitude}" + sprintf "%03d° ", long.to_i
      dec_min = (long - long.to_i.to_f) * 100 * 0.6
      result_longitude = "#{result_longitude}" + sprintf "%02d' ", dec_min.to_i
      dec_sec = (dec_min - dec_min.to_i.to_f) * 100 * 0.6

      sprintf "%s%02d''", result_longitude, dec_sec.to_i
    end

    def to_s
      get_dms_latitude + get_dms_longitude
    end

    def get_rad_lat
      latitude * PI / 180
    end

    def get_rad_long
      longitude * PI / 180
    end

    def calculate_heading(pos : TRS::Convert::Position)
      dec_long_self = -self.longitude
      dec_long_rem = -pos.longitude

      # delta_lat = (pos.latitude - self.latitude) * PI / 180 ## Useless???
      delta_lon = (dec_long_rem - dec_long_self) * PI / 180

      new_y = Math.sin(delta_lon) * Math.cos(pos.get_rad_lat)
      new_x = Math.cos(self.get_rad_lat) * Math.sin(pos.get_rad_lat) - Math.sin(self.get_rad_lat) * Math.cos(pos.get_rad_lat) * Math.cos(delta_lon)
      bearing = Math.atan2(new_y, new_x)
      if bearing <= 0
        bearing = (2 * PI + bearing)
      end
      ((bearing * 360) / (2 * PI)) % 360
    end

    def calculate_distance(pos : TRS::Convert::Position)
      lenght_long1 = Math.cos(self.get_rad_lat) * 60
      lenght_long2 = Math.cos(pos.get_rad_lat) * 60
      Math.hypot(((self.latitude - pos.latitude) * 60), ((self.longitude * lenght_long1) - (pos.longitude * lenght_long2)))
    end
  end
end
