# We need to be able to reed the sqlite database
require "sqlite3"

require "file_utils"

# CLI option parsing
require "option_parser"

# We want to work with the tools for Soprano
require "./TRS/**"
require "./TRS/Soprano/**"
require "./TRS/Convert/**"

include TRS::Soprano

scn_name : String = ""
scn_date : String = "1546300800" # 1 Janvier 2019
db_path : String? = nil
debug : Bool = false
pos_filter : String = ""  # " AND (latitude >= 36.70 AND latitude <= 44.8) AND (longitude >= -9.6 AND longitude <= 4.25)"
traj_filter : String = "" # " WHERE id IN (SELECT DISTINCT(fid) FROM positions WHERE (latitude >= 36.70 AND latitude <= 44.8) AND (longitude >= -9.6 AND longitude <= 4.25))"
pack : Bool = false
limit : Int64 = 0
rep_factor : UInt64 = 1
limit_sql = ""

start_time : Time = Time.utc
end_time : Time

def debug_print(data : String, debug : Bool = false)
  if debug
    STDERR.puts data
  end
end

def create_new_point(current_point : {Float64, Float64, Float64, Float64, Float64}, prev_point : {Float64, Float64, Float64, Float64, Float64})
  # 0 => latitude
  # 1 => longitude
  # 2 => speed
  # 3 => altitude
  # 4 => bearing
  pos_cur = TRS::Convert::Position.new(current_point[0], current_point[1])
  pos_old = TRS::Convert::Position.new(prev_point[0], prev_point[1])

  distance = pos_cur.calculate_distance(pos_old)

  diff_bear = (current_point[4] - prev_point[4]).abs % 360

  diff_spd = (current_point[2] - prev_point[2]).abs

  diff_alt = (current_point[3] - prev_point[3]).abs

  # If more than 10FL => create new point
  if diff_alt > 50
    return true
  end

  # If more than 20Kps => create new point
  if diff_spd > 50
    return true
  end

  # If more than 10° => create new point
  if diff_bear > 10
    return true
  end

  if distance > 100
    return true
  end
  false
end

main_parser = OptionParser.new do |parser|
  parser.banner = "Usage: generate-scenario [arguments]"
  parser.on("-i <path to sqlite db>", "--input=<path to sqlite db>", "Path to the sqlite database") { |path| db_path = path }
  parser.on("-s <scenario name>", "--output=<scenario name>", "Path to the sqlite database.") { |name| scn_name = name }
  parser.on("-d <scenario date>", "--date=<scenario date>", "Time/Date of the scenario") { |date| scn_date = date }
  parser.on("-f <sql filter>", "--trajfilter=<sql filter>", "Filter on trajectories") { |f| traj_filter = f }
  parser.on("-F <sql filter>", "--posfilter=<sql filter>", "Filter on position") { |f| pos_filter = f }
  parser.on("-l <nb max traj>", "--limit=<nb max traj>", "Maximum number of trajectories") { |l| limit = l.to_i64 }
  parser.on("-r <repetition factor>", "--repfactor=<repetition factor>", "Number of time trajectories are going to be repeated") { |r| rep_factor = r.to_u64 }
  parser.on("-p", "--pack", "All trajectories will start within 10 minutes") { pack = true }
  parser.on("-D", "--debug", "Activate debug traces") { debug = true }
  parser.on("-h", "--help", "Show this help") { puts parser; exit 0 }
end

main_parser.parse

if scn_name.size == 0
  puts main_parser.to_s
  exit 1
end

if !File.exists?(db_path.to_s)
  puts "SQLite Database doesn't exist. Please check the path and try again"
  exit 1
end

if limit > 0
  limit_sql = " LIMIT 0,#{limit} "
end

# Cleaning up before generating the scenario
FileUtils.rm_rf(scn_name)
if File.exists?("#{scn_name}_trad.log")
  File.delete("#{scn_name}_trad.log")
end
if File.exists?("#{scn_name}.scn")
  File.delete("#{scn_name}.scn")
end

puts "scn_name: #{scn_name}"

Dir.mkdir(scn_name, 0o755)

def_ac_type = "F16"

scenar = TRS::Soprano::Scenario.new
config = TRS::Soprano::Config.new(scn_name, scn_date.to_i64, scn_date.to_i64)
epitome = TRS::Soprano::Epitome.new(scn_name)
common = TRS::Soprano::CommonMvt.new(scn_name)
air_ptfs = TRS::Soprano::Airptf.new(scn_name)

db = DB.open("sqlite3://#{db_path}")

profile_cpt = 0
position_cpt = 0
begin
  # select id, icao24, callsign, first from flights WHERE id IN (SELECT DISTINCT(fid) FROM positions WHERE (latitude >= 36.70 AND latitude <= 42.8) AND (longitude >= -9.6 AND longitude <= 4.25)) order by first asc
  # profile = db.query("select id, icao24, callsign, first from flights WHERE id IN (SELECT DISTINCT(fid) FROM positions WHERE (latitude >= 36.70 AND latitude <= 42.8) AND (longitude >= -9.6 AND longitude <= 4.25) order by first asc")
  # SELECT id, icao24, callsign, first FROM flights ORDER BY first ASC
  # profile = db.query("SELECT id, icao24, callsign, first FROM flights #{traj_filter} ORDER BY first ASC #{limit_sql}")
  profile = db.query("SELECT id, icao24, callsign, first FROM flights #{traj_filter} ORDER BY first ASC #{limit_sql}")
  profile.each do
    profile_cpt += 1
    position_cpt = 0

    prev_speed = 0
    prev_altitude = 0

    p_id = profile.read(Int64)
    p_icao24 = profile.read(String)
    init_callsign : String = profile.read(String)
    p_first = profile.read(Int64)
    debug_print " profile callsign: #{init_callsign} ", debug
    rep_factor.times do |repetition_factor|
      # timestamp => {lat, long, speed, alt, beearing}
      last_pos_array = Hash(Int64, {Float64, Float64, Float64, Float64, Float64}).new
      rep_delta = (repetition_factor * (45 * 60))

      if pack
        time = Time.unix(p_first)
        # puts "Old time: #{time.to_s}"

        t1 = Time.utc(2019, 1, 1, 0, time.minute % 10, time.second)
        p_first = t1.to_unix + rep_delta

        # puts "New time: #{t1.to_s}"
      end

      p_callsign = "#{init_callsign}_#{repetition_factor}"

      epitome.add_plane_to_epitome(p_callsign)
      common.add_air_ptf_mvt(p_callsign)
      first_point = true
      debug_print "pid #{p_id}", debug

      puts "Profile[#{profile_cpt}]"

      position = db.query("select latitude, longitude, baro_altitude, velocity, true_track, squawk, timestamp from positions where fid=? #{pos_filter} order by timestamp asc", p_id)
      position.each do
        position_cpt += 1
        debug_print "Profile[#{profile_cpt}] Position[#{position_cpt}]", debug

        # pos_id = position.read(Int64) # never null
        pos_latitude = position.read(Float64)
        pos_longitude = position.read(Float64)
        pos_altitude = position.read(Float64?)
        pos_velocity = position.read(Float64?)
        pos_bearing = position.read(Float64?)
        # pos_roc = position.read(Float64?)
        pos_squawk = position.read(Int64?)
        # pos_spi = position.read(Int64?)
        pos_tstamp = position.read(Int64)

        if pack
          time = Time.unix(pos_tstamp)
          # puts "Old time: #{time.to_s}"

          t1 = Time.utc(2019, 1, 1, 0, time.minute % 10, time.second)
          pos_tstamp = t1.to_unix + rep_delta

          # puts "New time: #{t1.to_s}"
        end

        pos = TRS::Convert::Position.new(pos_latitude, pos_longitude)
        if !pos_altitude.nil?
          altitude = ((3.28084 * pos_altitude) / 100).to_i64
        else
          altitude = prev_altitude
        end

        if !pos_velocity.nil?
          speed = (1.94384 * pos_velocity).to_i64
          prev_speed = speed
        else
          speed = prev_speed
        end

        if pos_bearing.nil?
          bearing = 0.to_f64
        else
          bearing = pos_bearing
        end

        if pos_squawk.nil?
          squawk : Int64 = 0o7000
        else
          squawk = pos_squawk
        end

        if last_pos_array.size > 2
          debug_print "\tCompairing pos...", debug
          prev_pos_values = last_pos_array.last_value
          if !create_new_point({pos_latitude, pos_longitude, speed.to_f64, altitude.to_f64, bearing}, prev_pos_values)
            debug_print "\tIgnoring point", debug
            next
          end
        end

        last_pos_array[pos_tstamp] = {pos_latitude, pos_longitude, speed.to_f64, altitude.to_f64, bearing}

        debug_print "AirMvt.new", debug
        mvt = TRS::Soprano::AirMvt.new(
          pos.get_dms_latitude,
          pos.get_dms_longitude,
          bearing,
          "ALTI",
          altitude.to_i64,
          "HFT",
          "TAS",
          speed.to_i64,
          "K",
          squawk
        )

        epitome.add_point_to_epitome(speed, altitude, pos.get_soprano_latitude, pos.get_soprano_longitude)

        if first_point
          debug_print "add_air_ptf_init_point", debug
          common.add_air_ptf_init_point(pos_tstamp, mvt)

          debug_print "AirPlatform.new", debug
          ptf = TRS::Soprano::AirPlatform.new(
            p_callsign,
            def_ac_type,
            p_first,
            squawk,
            p_icao24
          )
          debug_print "air_ptfs.add_air_ptf", debug
          air_ptfs.add_air_ptf(ptf)

          scenar.add_profile(p_callsign, def_ac_type, "AFIL", "AFIL", [] of String)
          scenar.add_aptf(p_callsign, def_ac_type)

          debug_print "first_point", debug
          first_point = false
        else
          common.add_air_ptf_waypoint(mvt)
        end
      end
      position.close

      debug_print "common.end_air_ptf_mvt", debug
      common.end_air_ptf_mvt
      debug_print "epitome.add_land_base_to_epitome", debug
      epitome.add_land_base_to_epitome("AFIL")
    end
    debug_print "end loop", debug
  end
  # profile.close ?????
rescue excpt
  STDERR.puts "Exception!\n#{excpt.message}\n"
ensure
  debug_print "end loop 2 "
end
debug_print "Close DB"
db.close

debug_print "config.generate_xml", debug
config.generate_xml
debug_print "common.close_file", debug
common.close_file
air_ptfs.generate_xml
scenar.generate_file(scn_name, Time.unix(scn_date.to_i64))

epitome.close_epitome

generate_borders(scn_name)
generate_land(scn_name)
generate_summary(scn_name, profile_cpt)

end_time = Time.utc
STDOUT.puts "Elapsed time: #{end_time.to_unix - start_time.to_unix} seconds\n"
