#!/bin/bash

DIR=$1
OUTFILE=$2

echo "[" > "$OUTFILE";

for myf in "$DIR"/*; do
  cat "$myf" >> "$OUTFILE";
  echo -e "\n," >> "$OUTFILE";
done;

sed -i -e '$ s/^,$/]/' "$OUTFILE"