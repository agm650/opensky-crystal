#!/bin/bash

MEMSIZE=$1
RAM_SIZE=$(( 2048 * MEMSIZE ))
diskutil partitionDisk $( hdiutil attach -nomount ram://${RAM_SIZE} ) 1 GPTFormat APFS 'ramdisk' '100%'

