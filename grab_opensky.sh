#!/bin/bash
set -x

LAT_MIN=44.0
LAT_MAX=59.13
LONG_MIN=0.0
LONG_MAX=23.40

DURATION=60

BASENAME=adsb

for (( i = 0; i < ${DURATION}; i++ )); do
    DATE=$(date "+%Y-%m-%d_%H:%M");
    wget -q "https://opensky-network.org/api/states/all?lamin=${LAT_MIN}&lomin=${LONG_MIN}&lamax=${LAT_MAX}&lomax=${LONG_MAX}" -O "${BASENAME}_${DATE}.json"
    sleep 60;
done;