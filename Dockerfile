FROM alpine:latest AS builder

ADD . /build
WORKDIR /build:

RUN apk add crystal shards musl-dev openssl-dev pcre-dev readline-dev yaml-dev zlib-dev sqlite-static
RUN shards init && shards build --release --static
RUN (ls -1 && cat)

FROM alpine:latest

COPY --from=builder  /build/bin/generate-adsb /bin/generate-adsb
COPY --from=builder  /build/bin/generate-adsb /bin/generate-scenario
