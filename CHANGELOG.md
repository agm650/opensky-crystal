# Change Log

## 1.5.0 - 27th April 2020

* [Luc Dandoy (@agm650)](https://gitlab.com/agm650)
    * Cyclomatic Complexity improvement (ameba)
    * Improvement on Poseidon scenario size. Create point in trajectory only if it's far away from the previuous one or if speed/alt/heading is different enough.

## 1.1.0 - 11th August 2019

* [Luc Dandoy (@agm650)](https://gitlab.com/agm650)
    * Improvement on SQLite operation

## 1.0.0 - 21th February 2019

* [Luc Dandoy (@agm650)](https://gitlab.com/agm650)
    * First release of the tool to generate soprano scenarios
    * One tool is dedicated to populating an sqlite database with data from [opensky](https://opensky-network.org "opensky") adsb data
    * One tool is dedicated to generated the Soprano scenario from the previously populated sqlite database

