# adsb

Populating a database from adsb data in presented in json format(see the [opensky network](https://opensky-network.org/apidoc/rest.html))

## Installation

There's two main entry point for this tool:

  * generate-adsb : Tool to convert json data from opensky into a sqlite database
  * generate-scenario : Reuse the sqlite database to create the Soprano Scenario

## Usage

### SQLite database generation
In order to use the program you need to have a json file with a list of state vector. If you have multiple files, the merge-file.sh script can be used to concatenate everything into one big json file.

Once you have the input file you have to launch the generate-adsb executable. It will expect the following parameters:
```
Usage: generate-adsb [arguments]
    -i <path to json>, --input=<path to json>
                                     JSON input file to use to populate the database
    -o <sqlite_db_path>, --output=<sqlite_db_path>
                                     Path to the sqlite database.
    -p, --purge                      Do we want to purge an existing sqlite DB
    -h, --help                       Show this help
```
JSON input file and path to sqlite db are mandatory. Failure to provide them will make the program to exit without doing anything

The following command is enough to launch the conversion program:
`./generate-adsb -i test.json -o adsb.sqlite`

Note that the adsb.sqlite database doesn't have to exist. It will be created if needed by the program it self. If it already exist, you can specify that you want to clean it with the `-p` option

### Soprano scneario generation
Once the sqlite database have been populated, you can now generate the Soprano scenario. the generate-scenario executable is expecting the following parameters:
```
Usage: generate-scenario [arguments]
    -i <path to sqlite db>, --input=<path to sqlite db>
                                     Path to the sqlite database
    -s <scenario name>, --output=<scenario name>
                                     Path to the sqlite database.
    -d <scenario date>, --date=<scenario date>
                                     Time/Date of the scenario
    -h, --help                       Show this help
```

The only mandatory parameter here, is the path to the sqlite database.
If not specified otherwise the scenario name will be set to default and the scenario date will be the January 1st 2019.

## Development

Development is still in progress with some improvement planned:

*   spec tests for the Soprano library
*   Area Filtering when generating the scenario
*   Possibility to generate the network directly from the application
*   Others... 

Feel free to propose new ideas

## Contributing

1. Fork it (<https://github.com/your-github-user/adsb/fork>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Check your files with [ameba](https://github.com/veelenga/ameba)
4. Commit your changes (`git commit -am 'Add some feature'`)
5. Push to the branch (`git push origin my-new-feature`)
6. Create a new Pull Request

## Contributors

- Luc Dandoy- creator and maintainer
